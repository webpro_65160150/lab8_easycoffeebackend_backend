import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Product } from 'src/products/entities/product.entity';
import { Order } from './order.entity';

@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  total: number;

  @Column()
  qty: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  update: Date;

  @ManyToOne(() => Order, (order) => order.orderItems,{onDelete: 'CASCADE'})
  order: Order;

  @ManyToOne(() => Product, (product) => product.orderItems)
  product: Product;
}
